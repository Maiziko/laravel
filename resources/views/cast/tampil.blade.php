@extends('layout.master')

@section('title')
    Tampil Cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-sm btn-primary">Tambah</a>

    <table class="table">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $value)
    <tr>
      <th scope="row">{{$key+1}}</th>
      <td>{{$value->nama}}</td>
      {{-- <td>{{$value->umur}}</td> --}}
      {{-- <td>{{$value->bio}}</td> --}}
      <td>
        <form action="/cast/{{$value->id}}" method="POST">
            @csrf
            @method('delete')
            <a class = "btn btn-info btn-sm" href="/cast/{{$value -> id}}">Detail</a>
            <a class = "btn btn-warning btn-sm" href="/cast/{{$value -> id}}/edit">Edit</a>
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
        </form>
      </td>
    </tr>
    @empty
        <tr>
            <th>Tidak ada Cast</th>
        </tr>
    @endforelse
  </tbody>
</table>


@endsection