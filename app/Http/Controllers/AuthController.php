<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function welcome(Request $request)
    {
        $namadepan = $request->input('fname');
        $namabelakang = $request->input('lname');

        return view('page.welcome', ["namadepan" => $namadepan, "namabelakang" => $namabelakang]);
    }
}
