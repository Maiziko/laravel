<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    // Penambahan Halaman untuk merealisasikan Laravel CRUD 
    public function create()
    {
        return view('cast.tambah');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function store(Request $request)
    {
        // validasi data terisi atau tidak 
        $request->validate([
            'nama' => 'required|alpha|min:5',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio'),
        ]);

        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|alpha|min:5',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request->input('nama'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio'),
            ]);

        return redirect('/cast');
    }

    // Metode untuk menghapus Row Data pada tabel Cast
    public function destroy($id)
    {
        DB::table('cast')
            ->where('id', '=', $id)
            ->delete();

        return redirect('/cast');
    }
}
