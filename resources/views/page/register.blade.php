@extends('layout.master')
@section('title')
    Halaman Daftar
@endsection
@section('content')
    <h1> Buat Account Baru!</h1>
    <h3> Sign Up Form</h3>
    <!-- method "get" akan mencatat semua data yang diinput pada link urlnya (link yang berisi data ini mengambil value value dai tipe inputan)-->
    <!-- method get akan berbahaya apabila kita menginputkan data rahasia seperti Password-->
    <!-- method get akan menyusunkan query untuk memfilter hal yang diinginkan-->
    <!-- method "post" lebih aman untuk data yang sensitif karena tidak akan menampilkan values-->
    <form action="/welcome" method="POST">
    @csrf
    <label>First Name: </label><br><br>
    <input type="text" name="fname"><br><br>
    <label>Last name: </label><br><br><input type="text" name ="lname"><br><br>
    <label>Gender: </label><br><br>
    <!-- Fungsi name disini akan membuat opsi radio hanya bisa dipilih satu saja-->
    <input type="radio" value ="1" name="jenisKelamin">Male<br> 
    <input type="radio" value ="2" name="jenisKelamin">Female<br> 
    <input type="radio" value ="3" name="jenisKelamin">Other<br><br>
    <label>Nationality</label><br><br>
    <select name="Nationality">
        <option value="1">Indonesian</option>
        <option value="2">Singaporean</option>
        <option value="3">Malaysian</option>
        <option value="4">Australian</option>
    </select>
    <br><br>
    <label>Language Spoken: </label><br><br>
    <input type="checkbox" value="1" name ="bahasa">Bahasa Indonesia<br>
    <input type="checkbox" value="2" name ="bahasa">English<br>
    <input type="checkbox" value="3" name ="bahasa">Other<br>
    <br>
    <br>
        <label>Bio: </label><br><br>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" value ="Sign Up">
    </form>
@endsection